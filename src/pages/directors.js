import React, { useState } from 'react';
import Head from 'next/head';
import { useTranslation } from 'react-i18next';
import BasicLayout, {
  Top,
  Content,
  Main,
  Side,
  Bottom,
} from '@digigov/ui/layouts/Basic';
import FormBuilder, {
  Fieldset,
  FieldsetLabel,
  Field,

} from '@digigov/ui/form/Form';
import { SummaryList, SummaryListItem, SummaryListItemKey, SummaryListItemValue, SummaryListItemAction} from '@digigov/ui/core/SummaryList'
import Title from '@digigov/ui/typography/Title'
import Button from '@digigov/ui/core/Button'
import Header, { HeaderTitle } from '@digigov/ui/app/Header';
import ServiceBadge from '@digigov/ui/core/ServiceBadge';
import GovGRLogo from '@digigov/ui/govgr/Logo';
import GovGRFooter from '@digigov/ui/govgr/Footer';
import PageTitle, { PageTitleHeading } from '@digigov/ui/app/PageTitle';

export const fields = [
  {
    key: 'full_name',
    label: { primary: 'Full name'},
    required: true,
    type: 'string',
  },
  {
    key: 'phone',
    label: {
      primary: 'Mobile phone',
      secondary: 'A valid mobile number, eg. +306988848888',
    },
    required: true,
    type: 'mobile_phone',
  },
  {
    key: 'number',
    label: { primary: 'AFM' },
    required: true,
    width: '100px',
    type: 'int',
  },
];

export const addMoreFields = [
  {
    key: 'addmore',
    type: 'choice:single',
    required: true,
    extra: {
      options: [
        {
          label: { primary: 'Yes' },
          value: true,
        },
        {
          label: { primary: 'No' },
          value: false,
        },
      ],
    },
  },
];

const Directors = () => {
  const { t } = useTranslation();
  const [preview, setPreview] = useState(false);
  const [directors, setDirectors] = useState([]);
  return (
    <BasicLayout>
      <Head>
        <title>{t('app.name')}</title>
      </Head>
      <Top>
        <Header>
          <GovGRLogo />
          <HeaderTitle>
            {t('app.name')}
            <ServiceBadge label="ALPHA" />
          </HeaderTitle>
        </Header>
      </Top>
      <Content>
        <Main>
          { preview ? <>
              <Title size="xl">{`You have added ${directors.length} directors`}</Title>
              <SummaryList>
                { directors.map(director => (
                <SummaryListItem>
                  <SummaryListItemKey>Full Name</SummaryListItemKey>
                  <SummaryListItemValue>{director.full_name}</SummaryListItemValue>
                  <SummaryListItemAction>Change</SummaryListItemAction>
                  <SummaryListItemAction>Delete</SummaryListItemAction>
                </SummaryListItem>))}
              </SummaryList>

<FormBuilder
  key="addmore"
  fields={addMoreFields}
  onSubmit={(data) => {
    debugger
  }}
>
  <Fieldset>
    <FieldsetLabel>Do you want to add another director?</FieldsetLabel>
    {fields.map((field) => (
      <Field key={field.key} name={field.key} />
    ))}
  </Fieldset>
  <Button type="submit">Submit</Button>
    </FormBuilder>
              </>:
              <>
          <Title size="xl">What are the details for the director you want to add?</Title>
<FormBuilder
  key="addmore"
  fields={fields}
  onSubmit={(data) => {
    debugger;
    setPreview(true)
    setDirectors([...directors, data])
  }}
>
  <Fieldset>
    <FieldsetLabel>Director's details</FieldsetLabel>
    {fields.map((field) => (
      <Field key={field.key} name={field.key} />
    ))}
  </Fieldset>
  <Button type="submit">Submit</Button>
    </FormBuilder></>}
        </Main>
        <Side></Side>
      </Content>
      <Bottom>
        <GovGRFooter />
      </Bottom>
    </BasicLayout>
  );
};

export default Directors;
