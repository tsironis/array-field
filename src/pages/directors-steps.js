import { useContext } from 'react'
import Questions, {
  QuestionsContext,
  Step,
  StepForm,
  StepQuote,
  StepTitle,
  Field,
} from '@digigov/ui/app/Questions';
import { SummaryList, SummaryListItem, SummaryListItemKey, SummaryListItemValue, SummaryListItemAction } from '@digigov/ui/core/SummaryList';


const steps = [
  {
    name: 'directors',
    caption: 'Multiple directors',
    title: 'What are the details for the director you want to add?',
    description: 'lalal',
    fields: [
      {
        key: 'full_name',
        label: { primary: 'Full name' },
        required: true,
        type: 'string',
      },
      {
        key: 'phone',
        label: {
          primary: 'Mobile phone',
          secondary: 'A valid mobile number, eg. +306988848888',
        },
        required: true,
        type: 'mobile_phone',
      },
      {
        key: 'afm',
        label: { primary: 'AFM' },
        required: true,
        width: '100px',
        type: 'int',
      },

    ],
    type: 'array',
    review: {
      caption: 'multiple directors',
      title: 'You added the following directors',
      addMore: {
        title: 'Do you want to add more directors?',
        answers: {
          positive: { primary: 'Yes' },
          negative: { primary: 'No' }
        }
      },
    },
  },
  {
    name: 'intro',
    caption: 'intro.caption',
    title: 'intro.title',
    description: 'intro.description',
    fields: [
      {
        key: 'name',
        label: {
          primary: 'name.field.primary',
          secondary: 'name.field.secondary',
        },
        required: true,
        type: 'string',
      },
    ],
  },
  {
    name: 'time',
    nextStep: (data) => {
      if (data.time.age < 26) {
        return 'review';
      }
    },
    caption: 'time.caption',
    title: 'time.title',
    description: 'time.description',
    fields: [
      {
        key: 'age',
        label: {
          primary: 'age.field.primary',
          secondary: 'age.field.secondary',
        },
        required: true,
        type: 'int',
      },
    ],
  },
  {
    name: 'boomer',
    show: (data) => {
      return data.time.age >= 56;
    },
    caption: 'boomer.caption',
    title: 'boomer.title',
    description: 'boomer.description',
    fields: [
      {
        key: 'adult',
        label: {
          primary: 'adult.field.primary',
          secondary: 'adult.field.secondary',
        },
        required: true,
        type: 'string',
      },
    ],
  },
  {
    name: 'genx',
    show: (data) => {
      return data.time.age >= 41 && data.time.age < 56;
    },
    caption: 'genx.caption',
    title: 'genx.title',
    description: 'genx.description',
    fields: [
      {
        key: 'adult',
        label: {
          primary: 'adult.field.primary',
          secondary: 'adult.field.secondary',
        },
        required: true,
        type: 'string',
      },
    ],
  },
  {
    name: 'millenial',
    show: (data) => {
      return data.time.age >= 26 && data.time.age < 41;
    },
    caption: 'millenial.caption',
    title: 'millenial.title',
    description: 'millenial.description',
    fields: [
      {
        key: 'adult',
        label: {
          primary: 'adult.field.primary',
          secondary: 'adult.field.secondary',
        },
        required: true,
        type: 'string',
      },
    ],
  },
  {
    name: 'generic',
    caption: 'generic.caption',
    title: 'generic.title',
    description: 'generic.description',
    fields: [
      {
        key: 'info',
        label: {
          primary: 'info.field.primary',
          secondary: 'info.field.secondary',
        },
        required: true,
        type: 'string',
      },
    ],
  },
];

export default function DirectorsSteps(props) {
  return (
    <Questions
      steps={steps}
      onActiveStep={(step) => {
        console.log(step);
      }}
      translate={(key) => key}
      initialData={{ intro: { name: 'Νίκος Παπαδόπουλος' }, time: { age: 33 } }}
      initialStep={steps[0]}
      onChange={(data) => {
        console.log(data);
      }}
      onSubmit={(data) => {
        console.log(data, 'submit');
      }}
    >
      <Step name="directors">
        <StepTitle />
        <StepQuote>This is the intro</StepQuote>
        <StepForm submitButton={true}>
          <Field name="full_name" />
          <Field name="phone" />
          <Field name="afm" />
        </StepForm>
      </Step>
      <Step name="intro">
        <StepTitle />
        <StepQuote>This is the intro</StepQuote>
        <StepForm submitButton={true}>
          <Field name="name" />
        </StepForm>
      </Step>
      <Step name="time">
        <StepTitle />
        <StepQuote>This is the age</StepQuote>
        <StepForm submitButton={true}>
          <Field name="age" />
        </StepForm>
      </Step>
      <Step name="boomer">
        <StepTitle />
        <StepQuote>This is the conditioned step </StepQuote>
        <StepForm submitButton={true}>
          <Field name="adult" />
        </StepForm>
      </Step>
      <Step name="genx">
        <StepTitle />
        <StepQuote>This is the conditioned step </StepQuote>
        <StepForm submitButton={true}>
          <Field name="adult" />
        </StepForm>
      </Step>
      <Step name="millenial">
        <StepTitle />
        <StepQuote>This is the conditioned step </StepQuote>
        <StepForm submitButton={true}>
          <Field name="adult" />
        </StepForm>
      </Step>
      <Step name="generic">
        <StepTitle />
        <StepQuote>This is a normal step </StepQuote>
        <StepForm submitButton={true}>
          <Field name="info" />
        </StepForm>
      </Step>
    </Questions>
  )
}

const StepDirectors = () => {
  const { currentStep, submitStep, data, translate } = useContext(
    QuestionsContext
  );
  const directors = data.directors || [];
  return (
    <>
      <Step name="review-directors">
        <StepTitle />
        <SummaryList>
          {directors.map((director, idx) => (
            <SummaryListItem key={idx}>
              <SummaryListItemKey>Full Name</SummaryListItemKey>
              <SummaryListItemValue>{director.full_name}</SummaryListItemValue>
              <SummaryListItemAction>Change</SummaryListItemAction>
              <SummaryListItemAction>Delete</SummaryListItemAction>
            </SummaryListItem>))}
        </SummaryList>
      </Step>
    </>
  )
}